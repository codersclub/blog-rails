// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//= require_directory ../javascripts/templates

window.addEventListener('load', function() {
    var postsContainer = document.getElementById('posts'),
        addPostBtn = document.querySelector('button#add-post'),
        req = new XMLHttpRequest();

    postsContainer.addEventListener('click', function(e) {
        var clicked = e.target;
        if (clicked instanceof HTMLAnchorElement && clicked.dataset.method == 'delete') {
            var id = Number(clicked.dataset.id);

            deletePost(id, function () {
                var elem = postsContainer.querySelector('[data-id="' + id + '"]');

                elem.parentNode.removeChild(elem);
            });

            e.preventDefault();
        }
    });

    req.open('GET', '/posts.json', true);
    req.onreadystatechange = function(e) {
        if (req.readyState == 4) {
            var result = JSON.parse(req.responseText),
                viewString = '';

            result.forEach(function(elem) {
                viewString += JST['templates/post'](elem);
            });

            postsContainer.innerHTML = viewString;
        }
    };
    req.send(null);

    addPostBtn.addEventListener('click', function() {
        var data = {
            title: 'Some title',
            text: 'Some text'
        };
        addPost(data, function (e, response) {
            console.log(JST['templates/post'](response));
            console.log('dodano post', e, response);

            postsContainer.innerHTML += JST['templates/post'](response);
        });
    });

    function deletePost(id, cb) {
        var req = new XMLHttpRequest(),
            url = ('/posts/' + id);
        req.open('DELETE', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        setToken(req);

        req.onreadystatechange = function(e) {
            if (req.readyState == 4) {
                var response = JSON.parse(req.response);
                cb(e, response);
            }
        };
        req.send(null);
    }

    function addPost(data, cb) {
        var req = new XMLHttpRequest();

        req.open('POST', '/posts', true);
        req.setRequestHeader('Content-Type', 'application/json');
        setToken(req);

        req.onreadystatechange = function(e) {
            if (req.readyState == 4) {
                var response = JSON.parse(req.response);
                cb(e, response);
            }
        };
        req.send(JSON.stringify(data));
    }

    function setToken(xhr) {
        var token = document.querySelector('meta[name="csrf-token"]').content;
        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    }
});