class PostsController < ApplicationController
  
  #http_basic_authenticate_with name: 'dhh', password: 'secret',
  #except: [:index, :show]
  
  def index
    @posts = Post.all

    respond_to do |format|
      format.json { render json: @posts }
      format.html { render 'posts/index' }
    end
  end
  
  def new
    @post = Post.new
  end
  
  def create
    @post = Post.new(post_params)
    
    respond_to do |format|
      if @post.save
        format.json { render json: @post, status: :created, location: @post }
        format.html { redirect_to @post }
      else
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.html { render 'posts/new' }
      end
    end

  end
  
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.json { render json: @post }
      format.html { redirect_to posts_path }
    end
  end

  def show
    @post = Post.find(params[:id])

    respond_to do |format|
      format.json { render json: @post }
      format.html { render 'posts/show' }
    end
  end
  
  def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.find(params[:id])
    
    if @post.update(params[:post].permit(:title, :text))
      redirect_to @post
    else
      render 'edit'
    end
  end
  
  private
    def post_params
      params.require(:post).permit(:title, :text)
    end
end