class CommentsController < ApplicationController
  
  http_basic_authenticate_with name: 'dhh', password: 'secret', only: :destroy

  def index
    @post = Post.find(params.require(:post_id))

    respond_to do |format|
      format.json { render json: @post.comments }
    end
  end

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(params[:comment].permit(:commenter, :body))
    redirect_to post_path(@post)
  end
  
  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end
end
