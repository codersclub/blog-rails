Blog::Application.routes.draw do
  get "app/index"
  get "welcome/index"
  
  root 'welcome#index'
  
  resources :posts do
    resources :comments
  end
end
